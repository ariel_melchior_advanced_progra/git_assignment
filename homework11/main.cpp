#include <string>
#include <fstream>
#include <iostream>
#include<vector>
#include <thread>
#include "threads.h"



int main()
{
	call_I_Love_Threads();

	vector<int> primes1;
	getPrimes(1, 100, primes1);
	printVector(primes1);
	vector<int> primes3 = callGetPrimes(93, 289);
	cout << endl;
	printVector(primes3);
	vector<int> primes4 = callGetPrimes(0, 1000);
	cout << endl;
	printVector(primes4);
	vector<int> primes5 = callGetPrimes(0, 100000);
	cout << endl;
	printVector(primes5);
	vector<int> primes6 = callGetPrimes(0, 1000000);
	cout << endl;
	printVector(primes6);
	callWritePrimesMultipleThreads(0, 1000, "primes1.txt", 3);
	callWritePrimesMultipleThreads(0, 100000, "primes2.txt", 3);
	callWritePrimesMultipleThreads(0, 1000000, "primes3.txt", 3);

	system("pause");
	return 0;
}