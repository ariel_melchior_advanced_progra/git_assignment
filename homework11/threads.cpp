﻿#include <string>
#include <fstream>
#include <iostream>
#include<vector>
#include <thread>
#include <time.h>
#include"threads.h"
using namespace std;


void I_Love_Threads()
{
	cout << "i love threads" << endl;
}

void call_I_Love_Threads()
{
	thread t1(I_Love_Threads);
	t1.join();
}
void getPrimes(int begin, int end, vector<int>& primes)
{
	bool isPrime = true;
	for (int i = begin; i <= end; ++i)
	{
		for (int j = 2; j <= i / 2; ++j)
		{
			if (i%j == 0)
			{
				isPrime = false;
				break;
			}
		}
		if (isPrime)
		{
			primes.push_back(i);
		}
		isPrime = true;
	}

}
void printVector(vector<int> primes)
{
	for (std::vector<int>::const_iterator i = primes.begin(); i != primes.end(); ++i)
	{
		std::cout << *i << endl;
	}
}

vector<int> callGetPrimes(int begin, int end)
{
	vector<int> primes1;
	thread t1(getPrimes,begin,end,ref(primes1));
	t1.join();
	return primes1;
}

void writePrimesToFile(int begin, int end, ofstream& file)
{
	
	bool isPrime = true;
	for (int i = begin; i <= end; ++i)
	{
		for (int j = 2; j <= i / 2; ++j)
		{
			if (i%j == 0)
			{
				isPrime = false;
				break;
			}
		}
		if (isPrime)
		{
			file << i << endl;
		}
		isPrime = true;
	}
	
}

void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	clock_t start = clock();
	int j = 0;
	std::thread *t = new std::thread[N];
	ofstream myfile;
	myfile.open(filePath);
	
	
		for (int i = begin; i <= end- ((end - begin) / N); i = i + ((end - begin) / N))
		{
			t[j] = std::thread(writePrimesToFile, i, i + ((end - begin) / N), ref(myfile));

			j++;
			
		}
		for (j = 0; j < N; j++)
		{
			t[j].join();
		}

	
	
	myfile.close();
	clock_t stop = clock();
	double elapsed = (double)(stop - start)* 1000.0 / CLOCKS_PER_SEC;
	cout<<"Time elapsed in ms:"<<elapsed<<endl;
	
}